#!/usr/bin/env python
import paho.mqtt.client as mqtt
import os

def on_connect(client, userdata, flags, rc):
    print("Connected with result code " + str(rc))

    #client.subscribe("owrtwifi/status/mac-ff-ff-ff-ff-ff-ff/event")
    client.subscribe("owrtwifi/status/+/event")

def on_message(client, userdata, msg):
    print(msg.topic + " " + str(msg.payload))
    a = str(msg.payload)

    if 'ff-ff-ff-ff-ff-ff' in msg.topic and 'new' in msg.payload:
        os.system("./speech.sh 'Sebastian join WiFi'")
    elif 'ff-ff-ff-ff-ff-ff' in msg.topic and 'del' in msg.payload:
        os.system("./speech.sh 'Sebastian leave WiFi'")

    else:
        if a == 'new':
            os.system("espeak 'Unknown client join WiFi'")
        elif a == 'del':
	    os.system("espeak 'Unknown client leave WiFi'")
        else:
            print(str(msg.payload))


client = mqtt.Client()
client.on_connect = on_connect
client.on_message = on_message

client.connect("localhost", 1883, 60)

client.loop_forever()
